<?php

namespace Drupal\odometer_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'Odometer Field' formatter.
 *
 * @FieldFormatter(
 *   id = "odometer_field_formatter",
 *   label = @Translation("Odometer"),
 *   field_types = {
 *     "integer",
 *     "decimal"
 *   }
 * )
 */
class OdometerFieldFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'duration' => 3000,
      'theme' => 'default',
    ] + parent::defaultSettings();
  }


  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('@setting: @value', [
      '@setting' => 'Duration',
      '@value' => $this->getSetting('duration')]
    );

    $summary[] = $this->t('@setting: @value', [
      '@setting' => 'Theme',
      '@value' => $this->getSetting('theme')]
    );

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements['duration'] = [
      '#type' => 'number',
      '#title' => $this->t('Duration'),
      '#default_value' => $this->getSetting('duration'),
      '#weight' => 0,
    ];

    $theme_options = [
      'default'  => $this->t('Default'),
      'minimal' => $this->t('Minimal'),
      'car' => $this->t('Car'),
      'plaza' => $this->t('Plaza'),
      'slot-machine' => $this->t('Slot Machine'),
      'train-station' => $this->t('Train Station'),
      'digital' => $this->t('Digital'),
    ];

    $elements['theme'] = [
      '#type' => 'select',
      '#title' => $this->t('Theme'),
      '#options' => $theme_options,
      '#default_value' => $this->getSetting('theme'),
      '#weight' => 1,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $theme_libraries = [
      'default' => 'odometer_field/theme_default',
      'minimal' => 'odometer_field/theme_minimal',
      'car' => 'odometer_field/theme_car',
      'plaza' => 'odometer_field/theme_plaza',
      'slot-machine' => 'odometer_field/theme_slot_machine',
      'train-station' => 'odometer_field/theme_train_station',
      'digital' => 'odometer_field/theme_digital',
    ];

    foreach ($items as $delta => $item) {
      // Render each element with the Odometer setting options.
      $elements[$delta] = [
        '#theme' => 'odometer_field_formatter',
        '#odometer_settings' => [
          'duration' => $this->getSetting('duration'),
          'theme' => $this->getSetting('theme'),
        ],
        '#value' => $item->value,
        '#attached' => [
          'library' => [
            'odometer_field/base',
            $theme_libraries[$this->getSetting('theme')], // @todo: validate if exists.
          ],
        ],
      ];
    }

    return $elements;
  }

}
