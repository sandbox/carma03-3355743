/**
 * @file
 * JavaScript file for the Odometer Field Formatter module.
 */

(function ($, Drupal, drupalSettings) {

  Drupal.behaviors.odometer_field = {
    attach: function attach(context, settings) {
      // @todo: add code here
      window.odometerOptions = {
        auto: false, // Don't automatically initialize everything with class 'odometer'
        //selector: '.my-numbers', // Change the selector used to automatically find things to be animated
        //format: '(,ddd).dd', // Change how digit groups are formatted, and how many digits are shown after the decimal point
        //duration: 3000, // Change how long the javascript expects the CSS animation to take
        //theme: 'car', // Specify the theme (if you have more than one theme css file on the page)
        animation: 'count' // Count is a simpler animation method which just increments the value,
                           // use it when you're looking for something more subtle.
      };

      const $odometer_elems = $('[data-odometer]', context);
      $odometer_elems.each(function() {
        const $elem = $(this);
        let value = $elem.data('odometer-value');
        const o = new Odometer({
            el: this,
            value: 0,
            theme: $(this).data('odometer-theme'),
            format: '',
            duration: 9500,
        });
        o.render();
        o.update(value);
      });
    }
  };

})(jQuery, Drupal, drupalSettings);
